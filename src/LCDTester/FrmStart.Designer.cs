﻿namespace LCDTester
{
    partial class FrmStart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gbRGB = new System.Windows.Forms.GroupBox();
            this.tbB = new System.Windows.Forms.TrackBar();
            this.tbG = new System.Windows.Forms.TrackBar();
            this.tbR = new System.Windows.Forms.TrackBar();
            this.tmrMove = new System.Windows.Forms.Timer(this.components);
            this.gbRGB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbR)).BeginInit();
            this.SuspendLayout();
            // 
            // gbRGB
            // 
            this.gbRGB.Controls.Add(this.tbB);
            this.gbRGB.Controls.Add(this.tbG);
            this.gbRGB.Controls.Add(this.tbR);
            this.gbRGB.Location = new System.Drawing.Point(12, 12);
            this.gbRGB.Name = "gbRGB";
            this.gbRGB.Size = new System.Drawing.Size(522, 252);
            this.gbRGB.TabIndex = 0;
            this.gbRGB.TabStop = false;
            // 
            // tbB
            // 
            this.tbB.Location = new System.Drawing.Point(17, 175);
            this.tbB.Name = "tbB";
            this.tbB.Size = new System.Drawing.Size(479, 69);
            this.tbB.TabIndex = 4;
            this.tbB.ValueChanged += new System.EventHandler(this.Tb_ValueChanged);
            // 
            // tbG
            // 
            this.tbG.Location = new System.Drawing.Point(17, 100);
            this.tbG.Name = "tbG";
            this.tbG.Size = new System.Drawing.Size(479, 69);
            this.tbG.TabIndex = 3;
            this.tbG.ValueChanged += new System.EventHandler(this.Tb_ValueChanged);
            // 
            // tbR
            // 
            this.tbR.Location = new System.Drawing.Point(17, 25);
            this.tbR.Name = "tbR";
            this.tbR.Size = new System.Drawing.Size(479, 69);
            this.tbR.TabIndex = 2;
            this.tbR.ValueChanged += new System.EventHandler(this.Tb_ValueChanged);
            // 
            // tmrMove
            // 
            this.tmrMove.Enabled = true;
            this.tmrMove.Interval = 10000;
            this.tmrMove.Tick += new System.EventHandler(this.TmrMove_Tick);
            // 
            // FrmStart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.gbRGB);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmStart";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmStart_Load);
            this.gbRGB.ResumeLayout(false);
            this.gbRGB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbR)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbRGB;
        private System.Windows.Forms.TrackBar tbB;
        private System.Windows.Forms.TrackBar tbG;
        private System.Windows.Forms.TrackBar tbR;
        private System.Windows.Forms.Timer tmrMove;
    }
}

