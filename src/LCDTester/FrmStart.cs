﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace LCDTester
{
    public partial class FrmStart : Form
    {
        public FrmStart()
        {
            InitializeComponent();
        }

        private void FrmStart_Load(object sender, EventArgs e)
        {
            InitRGB();
        }

        private void Tb_ValueChanged(object sender, EventArgs e)
        {
            SetRGB();
        }

        private void TmrMove_Tick(object sender, EventArgs e)
        {
            MoveRGB();
        }

        private void InitRGB()
        {
            try
            {
                const int max = 255;

                tbR.Maximum = max;
                tbG.Maximum = max;
                tbB.Maximum = max;

                tbR.LargeChange = max;
                tbG.LargeChange = max;
                tbB.LargeChange = max;

                SetRGB();
            }
            catch (Exception) { }
        }

        private void MoveRGB()
        {
            try
            {
                const int space = 30;

                var aRandom = new Random();

                var x = aRandom.Next(Width - gbRGB.Width - space);
                var y = aRandom.Next(Height - gbRGB.Height - space);

                gbRGB.Top = y;
                gbRGB.Left = x;
            }
            catch (Exception) { }
        }

        private void SetRGB()
        {
            try
            {
                var r = tbR.Value;
                var g = tbG.Value;
                var b = tbB.Value;

                var color = Color.FromArgb(r, g, b);

                this.BackColor = color;
            }
            catch (Exception) { }
        }
    }
}
