# LCDTester

## Getting started

> ToDo: Chapters below to be removed

> Hint To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

### Add your files

- [x] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [x] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/Eratosthenes/lcdtester.git
git branch -M main
git push -uf origin main
```

### Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/Eratosthenes/lcdtester/-/settings/integrations)

### Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

### Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

## Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

> ToDo: Previous chapters to be removed

## Name

LCDTester

## Description

A tiny .Net 3.0 program to test LCD screens having different effects and faults.

The program has been written as my LCD screen has had *burn in effects*. The goal was to provide some kind of "LCD Provisioner". The tool also should provide an easy option to verify if the burn in effect has vanished.

## Visuals

> ToDo: Provide information

Find here a very impressive screenshot of the application...

![Screenshot][screenshot]

> Hint: Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation

The installation is as simple as possible. Just copy the executable (LCDTester.exe) found in the **bin** folder to your PC or build the provided Visual Studio Solution found in **source** and execute the stuff.

## Usage

> ToDo: Provide more detailed information

Execute the binary (LCDTester.exe) by double clicking it or run it via Visual Studio.

A full screen painted black should open containing three sliders. The sliders will move every several seconds on the screen to avoid any kind of burn in.

The value of the sliders can be changed via mouse or keyboard.

When using the Keyboard the focus will change from one slider to the next. Order from top to bottom or vice versa:

- Red
- Green
- Blue

The backcolor of the screen will change immediately when a slider position has changed.

Keyboard controls:

| Key | Effect | Comment |
| :- | :- | :- |
| ALT+F4 | Exit the Program | Most important feature *This should be known*... ;-) |
| TAB | Roll through the sliders | Order: R, G, B (reverse using SHIFT-TAB) |
| Cursor-Up, Cursor-Right | Increase the value of the slider | Tiny step |
| Cursor-Down, Cursor-Left | Decrease the value of the slider | Tiny step |
| POS1, Page-Up | Minimum value | 0 in case of RGB |
| END, Page-Down | Maximum value | 255 in case of RGB |

## Support

> ToDo: Provide information

## Roadmap

> ToDo: Provide information

There could be any kind of roadmap but nothing is planned yet.

## Contributing

> ToDo: Provide information

> Hint: State if you are open to contributions and what your requirements are for accepting them.

> Hint: For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

> Hint: You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment

> ToDo: Provide information

At the moment there is none at all.

Show your appreciation to those who have contributed to the project.

## License

MIT - see the attached LICENSE

## Project status

> ToDo: Provide information

> Hint: If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

[screenshot]: ./docs/lcd_tester_screenshot.png "LCDTester screenshot"
